﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_GameOfLife
{
    class Program
    {
        static int xSize = 5, ySize = 5;
        bool allDead = false;
        int[,] tab;
        int[,] tabNew;
        static void Main(string[] args)
        {
            Program prog = new Program();
            prog.showTable();

            while (!prog.end())
            {
                Console.ReadKey();
                prog.copyOldTable();
                prog.checkingUpdate();
                prog.copyNewTable();
                prog.showTable();
            }

            Console.ReadKey();
        }

        void generateTable()
        {
            Random rand = new Random();

            for (int i = 0; i < xSize + 2; i++)
            {
                for (int j = 0; j < ySize + 2; j++)
                {
                    tab[i, j] = 2;
                }
            }

            for (int i = 1; i < xSize + 1; i++)
            {
                for (int j = 1; j < ySize + 1; j++)
                {
                    tab[i, j] = rand.Next(0, 2);
                }
            }

        }
        void showTable()
        {
            Console.Clear();
            for (int i = 0; i < xSize + 2; i++)
            {
                for (int j = 0; j < ySize + 2; j++)
                {
                    if (tab[i, j] != 2)
                    {
                        Console.Write( (tab[i, j] == 1 ? "*" : ".") );
                    }
                }
                Console.Write("\n");
            }
        }
        void checkingUpdate()
        {
            for (int i = 1; i < xSize + 1; i++)
            {
                for (int j = 1; j < ySize + 1; j++)
                {
                    CheckEverything(i, j);
                }

            }
        }
        void CheckEverything(int x, int y)
        {
            int quantity = CheckAliveQuantity(x, y);
            if (tab[x, y] == 1)
            {
                if (quantity < 2) tabNew[x, y] = 0; // osamotniona
                if (quantity > 3) tabNew[x, y] = 0; // przeludnienie
            }
            if (tab[x, y] == 0)
            {
                if (quantity == 3) tabNew[x, y] = 1; //reprodukcja
            }

        }

        int CheckAliveQuantity(int x, int y)
        {
            int counter = 0;
            for (int i = x - 1; i < x + 2; i++)
            {
                for (int j = y - 1; j < y + 2; j++)
                {
                    if (x == i && y == j) { }
                    else
                    {
                        if (tab[i, j] == 1)
                        {
                            counter++;
                        }
                    }

                }
            }
            //Console.WriteLine("Dla"+x+" "+y+" : "+counter);
            return counter;
        }
        void copyOldTable()
        {
            for (int i = 0; i < xSize + 2; i++)
            {
                for (int j = 0; j < ySize + 2; j++)
                {
                    tabNew[i, j] = tab[i, j];
                }

            }
        }
        void copyNewTable()
        {
            allDead = true;
            for (int i = 0; i < xSize + 2; i++)
            {
                for (int j = 0; j < ySize + 2; j++)
                {
                    if (tabNew[i, j] == 1) allDead = false;
                    tab[i, j] = tabNew[i, j];
                }

            }
        }

        bool end()
        {
            return allDead;
        }

        Program()
        {
            Console.Write("Wysokosc (domyslnie 5): ");
            string answer = Console.ReadLine();
            if (!answer.Equals(""))
                xSize = int.Parse(answer);

            Console.Write("Szerokosc (domyslnie 5): ");
            answer = Console.ReadLine();
            if (!answer.Equals(""))
                ySize = int.Parse(answer);

            tab = new int[xSize + 2, ySize + 2];
            tabNew = new int[xSize + 2, ySize + 2];

            generateTable();
        }


    }
}
